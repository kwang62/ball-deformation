using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Runtime.InteropServices;
using System;
//using MathNet.Numerics;
//using MathNet.Numerics.LinearAlgebra;

public class HapticInjection : HapticClassScript {
	
	//Generic Haptic Functions
	private GenericFunctionsClass myGenericFunctionsClassScript;
	private HapticProperties hapticProperties; 
	private float myDopLimit;
	GameObject human,needle,   marker,marker2,marker3;
	int frame;
	bool TouchFrameCur,TouchFrameNxt,CollidePrev,CollideCur;
	Vector3 PrevPos,CurPos;
	Vector3[] positions,PrevTriangle,CurTriangle;
	public int DeformOrCut;  //0 deform;1 cut
	Point PointInfo;  // contains the surounding triangle of a vertex
    fea_second fea;
	DeleteElem DeElem;
	FindNeigborForMatlab neigbor;
	/*****************************************************************************/
	
	void Awake()
	{
		myGenericFunctionsClassScript = transform.GetComponent<GenericFunctionsClass>();
        DeformOrCut = 10;
	}
	
	
	void Start()
	{
		positions = new Vector3[2];
		marker = GameObject.Find ("InjectionMarker1");
		marker2 = GameObject.Find ("InjectionMarker2");
			marker3=GameObject.Find ("InjectionMarker3");
		frame = 1;
		hapticProperties = GameObject.Find ("dummy").GetComponent<HapticProperties>();
		human = GameObject.Find("MaleFigure");needle = GameObject.Find ("tip");
		PrevPos = CurPos = new Vector3 (0f,0f,0f);
		CollidePrev = CollideCur = false;
        if (DeformOrCut == 1)
		    PointInfo = new Point(Cut.deformingMesh.triangles);
		if(PluginImport.InitHapticDevice())
		{
			Debug.Log("OpenGL Context Launched");
			Debug.Log("Haptic Device Launched");
			
			myGenericFunctionsClassScript.SetHapticWorkSpace();
			myGenericFunctionsClassScript.GetHapticWorkSpace();
			
			//Update Workspace as function of camera
			PluginImport.UpdateWorkspace(myHapticCamera.transform.rotation.eulerAngles.y);  //the rotation of a haptic camer
			//		Debug.Log("My haptic camera is: " + myHapticCamera.transform.rotation.eulerAngles.y);
			//Set Mode of Interaction
			/*
			 * Mode = 0 Contact
			 * Mode = 1 Manipulation - So objects will have a mass when handling them
			 * Mode = 2 Custom Effect - So the haptic device simulate vibration and tangential forces as power tools
			 * Mode = 3 Puncture - So the haptic device is a needle that puncture inside a geometry
			 */
			PluginImport.SetMode(ModeIndex);
			//Show a text descrition of the mode
			myGenericFunctionsClassScript.IndicateMode();
			
			//Set the lenght of the syringue needle to penetrate inside the tissue
			PluginImport.SetMaximumPunctureLenght(maxPenetration);//Debug.Log(maxPenetration);
			
			//Set the touchable face(s)
			PluginImport.SetTouchableFace(ConverterClass.ConvertStringToByteToIntPtr(TouchableFace));
			
			
		}
		else
			Debug.Log("Haptic Device cannot be launched");
		
		
		/***************************************************************/
		//Set Environmental Haptic Effect
		/***************************************************************/
		
		// Constant Force Example - We use this environmental force effect to simulate the weight of the cursor
		//myGenericFunctionsClassScript.SetEnvironmentConstantForce();
		
		/***************************************************************/
		//Setup the Haptic Geometry in the OpenGL context
		/***************************************************************/
		myGenericFunctionsClassScript.SetHapticGeometry();
		
		//Get the Number of Haptic Object
		//Debug.Log ("Total Number of Haptic Objects: " + PluginImport.GetHapticObjectCount());
		
		/***************************************************************/
		//Launch the Haptic Event for all different haptic objects
		/***************************************************************/
		PluginImport.LaunchHapticEvent();

        fea = new fea_second();
        
		
	
	}
	
	void Update()
	{

		frame *= -1;
        if (DeformOrCut == 1)
		    PointInfo = new Point(Cut.deformingMesh.triangles);
		/***************************************************************/
		//Update Workspace as function of camera
		/***************************************************************/
		PluginImport.UpdateWorkspace(myHapticCamera.transform.rotation.eulerAngles.y);
		
		/***************************************************************/
		//Update cube workspace
		/***************************************************************/
		myGenericFunctionsClassScript.UpdateGraphicalWorkspace();
		
		/***************************************************************/
		//Haptic Rendering Loop
		/***************************************************************/
		PluginImport.RenderHaptic ();
		
		myGenericFunctionsClassScript.GetProxyValues();
		
		myGenericFunctionsClassScript.GetTouchedObject();
		
		//	Debug.Log (PluginImport.GetTouchedObjectId());
		//For the Puncture Mode effect
		//double[] torque = ConverterClass.ConvertIntPtrToDouble3 (PluginImport.GetProxyTorque ());
		
		//Debug.Log(ConverterClass.ConvertDouble3ToVector3(torque));

	/*	if (PluginImport.GetContact ()) 
		{
			Debug.Log("contact");
		}
		else
			Debug.Log("no contact"); */

		

		//if(PluginImport.GetMode() == 3)
		{
			//Debug.Log ("Contact state is set to " + PluginImport.GetContact());
			//Debug.Log ("Penetration State " + PluginImport.GetPenetrationRatio());
			
			double[] myScp = new double[3];
			myScp = ConverterClass.ConvertIntPtrToDouble3(PluginImport.GetFirstScpPt());
			
			double[] myPinch = new double[3];
			myPinch = ConverterClass.ConvertIntPtrToDouble3(PluginImport.GetProxyDirection());
			
			Vector3 start;
			start = needle.transform.position;
			Vector3 end = new Vector3();
			end = ConverterClass.ConvertDouble3ToVector3(myPinch);
			end.Normalize();
			//Debug.Log(end);
			//GameObject.Find ("InjectionMarker1").transform.position = start+end * maxPenetration;
			
			//string TouchObject = 
			
			//	Ray inputRay = new Ray(start,end);
			RaycastHit Hit;
			//	if(Physics.Raycast(inputRay,out Hit))
			//			Debug.Log ("RaycastHit");

			//Ray ast so we can determine the limitation of the puncture
			RaycastHit[] hits;
			hits = Physics.RaycastAll(start,end, maxPenetration);//Debug.Log("hit length: "+hits.Length);
			//if(hits.Length!=0)
			//	Debug.Log("hit length: "+hits.Length);
			float forceOffset = 0.1f;
			float force = 5f;
			float kd = 0.1f;
			//	int ObjectID = PluginImport.GetTouchedObjectId();
			//	if(ObjectID!=-1)
			double[] proxy = ConverterClass.ConvertIntPtrToDouble3(PluginImport.GetProxyPosition());
			Vector3 ProxyPos = ConverterClass.ConvertDouble3ToVector3(proxy);
			CurPos = ProxyPos;
			if(Time.frameCount<=2)
				positions[0]=positions[1]=Vector3.zero;
			else
				positions[Time.frameCount%2] = ProxyPos;
			Vector3 dir = positions[1]-positions[0];//Debug.Log(dir+" "+CompareVector(dir, new Vector3(0, 0, 0)));
            
			maxPenetration = 0.4f;
            fea.CollisionDetection(start, start + end * maxPenetration);
			//	GameObject.Find("InjectionMarker3").transform.position = start + end * maxPenetration;
			/*	GameObject blood = GameObject.Find("blood");
				blood.transform.position = start + end * maxPenetration;
				if(blood==null)
					Debug.Log("blood is null");
				DeElem = new DeleteElem(fea,start,start + end * maxPenetration,blood); */
			//	neigbor = new FindNeigborForMatlab();
			//	neigbor.FindNeighbor(fea,start,start + end * maxPenetration);

            PrevPos = CurPos;
			
		}

	}
	
	


	void OnDisable()
	{
		if (PluginImport.HapticCleanUp())
		{
			Debug.Log("Haptic Context CleanUp");
			Debug.Log("Desactivate Device");
			Debug.Log("OpenGL Context CleanUp");
		}
	}
	
	
	void SetPunctureStack(int nbLayer ,string[] name, float[] array)
	{
		IntPtr[] objname = new IntPtr[nbLayer];
		//Assign object encounter along puncture vector to the Object array
		for (int i = 0; i < nbLayer; i++)
			objname[i] = ConverterClass.ConvertStringToByteToIntPtr(name[i]);
		
		PluginImport.SetPunctureLayers(nbLayer, objname,ConverterClass.ConvertFloatArrayToIntPtr(array));
		
	}

   public static bool CompareVector(Vector3 v1, Vector3 v2)
    {
        if (v1.x == v2.x && v1.y == v2.y && v1.z == v2.z)
            return true;
        return false;
    }
}
