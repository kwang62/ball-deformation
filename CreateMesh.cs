using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
public class CreateMesh : MonoBehaviour
{
    public static ArrayList vertices;
    public static List<int> faces;
    public static double DeformationRange;
    public static MeshFilter meshFilter;
    private ArrayList vertices_copy;
    void Awake()
    {
        Read(); 
        GameObject obj = new GameObject("object");
        meshFilter = (MeshFilter)obj.AddComponent(typeof(MeshFilter));
        meshFilter.mesh = ConstructMesh();
        MeshRenderer renderer = obj.AddComponent(typeof(MeshRenderer)) as MeshRenderer;
        renderer.material.shader = Shader.Find("Particles/Diffuse");
        Texture2D tex = new Texture2D(1, 1);
        tex.SetPixel(0, 0, Color.green);
        tex.Apply();
        renderer.material.mainTexture = tex;
        renderer.material.color = Color.green;
		renderer.material.shader = Shader.Find ("Custom/HumanHead");
		//renderer.material.SetTexture ("_MainTex","ASHSEN_3");
	//	renderer.material.mainTexture = Resources.Load("human_skin2") as Texture;
		gameObject.AddComponent ("HapticProperties");
    }

    void Update()
    {
        ResetMesh();
        meshFilter.mesh.Clear();
        meshFilter.mesh.vertices = ConverterClass.ConvertArrayListToVector3(vertices);
        meshFilter.mesh.triangles = faces.ToArray();
        meshFilter.mesh.RecalculateNormals();
    }

    void Read()
    {
        FileInfo theSourceFile = null;
        StreamReader reader = null;
        string text = " ";
        Vector3 vertex;

        theSourceFile = new FileInfo(" resources/HandNode.txt");
        reader = theSourceFile.OpenText();
        text = reader.ReadLine();
        vertices = new ArrayList();
        vertices_copy = new ArrayList();
        faces = new List<int>();

        vertex = new Vector3(0f,0f,0f);
        vertices.Add(vertex);
        vertices_copy.Add(vertex);

        while (text != null)
        {
            string[] arr;
            arr = text.Split(' ');
		//	vertex = new Vector3(-float.Parse(arr[0]), -float.Parse(arr[1]), float.Parse(arr[2]));
			vertex = new Vector3(-float.Parse(arr[0])*.01f, -float.Parse(arr[1])*.01f, float.Parse(arr[2])*.01f);
            vertices.Add(vertex);
            vertices_copy.Add(vertex);
            //	Debug.Log(vertex);
            text = reader.ReadLine();//Debug.Log(vertex[0]+" "+vertex[1]+" "+vertex[2]);
        }

		reader.Close ();
        theSourceFile = new FileInfo(" resources/hand_surface.txt");
        reader = theSourceFile.OpenText();
        text = reader.ReadLine();

        while (text != null)
        {
            string[] arr;
            arr = text.Split(' ');
            faces.Add(int.Parse(arr[0]));
            faces.Add(int.Parse(arr[1]));
            faces.Add(int.Parse(arr[2]));
           // Debug.Log(int.Parse(arr[0]) + " " + int.Parse(arr[1]) + " " + int.Parse(arr[2]));
            text = reader.ReadLine();//Debug.Log(vertex[0]+" "+vertex[1]+" "+vertex[2]);
        }

		reader.Close ();
    }

    // reset the position of the mesh back to its original position every constant frames
    private void ResetMesh()
    {
        if (Time.frameCount % 61 != 0)
            return;
        vertices = new ArrayList(vertices_copy);
        //Debug.Log("reset");
     /*   for (int i = 1; i < fea_second.Points.Count; i++)
        {
          //  vertices[i] = vertices_copy[i];
            for (int j = 0; j < 3;j++)
                vertices[i][j] = vertices_copy[i][j];
            i++;
        } */
    }

    Mesh ConstructMesh()
    {
        Mesh m = new Mesh();
        m.name = "FEA_Object";
       
        int count = 0;
                
        m.vertices = ConverterClass.ConvertArrayListToVector3(vertices);
        m.triangles = faces.ToArray();
        Vector2[] uvs = new Vector2[vertices.Count];
        for (int i = 0; i < uvs.Length; i++)
        {
            uvs[i] = new Vector2(m.vertices[i].x, m.vertices[i].z);
        }
        m.uv = uvs;
        m.RecalculateNormals();

        return m;
    }

    Mesh Create(float width, float height)
    {
        Mesh m = new Mesh();
        m.name = "ScriptedMesh";
        m.vertices = new Vector3[] {
         new Vector3(-width, -height, 0.01f),
         new Vector3(width, -height, 0.01f),
         new Vector3(width, height, 0.01f),
         new Vector3(-width, height, 0.01f)
     };
        m.uv = new Vector2[] {
         new Vector2 (0, 0),
         new Vector2 (0, 1),
         new Vector2(1, 1),
         new Vector2 (1, 0)
     };
        m.triangles = new int[] { 0, 1, 2, 0, 2, 3 };
        m.RecalculateNormals();

        return m;
    }
}
