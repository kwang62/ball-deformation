using UnityEngine;
using System.Collections;
using System;
using System.IO;
using System.Linq;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;
using System.Collections.Generic;
using Mehroz;
using MathNet.Numerics.Data.Text;

public class fea_second : MonoBehaviour  
{
	
	public fea_second()
	{
		Elements = new List<element>();
		Points = new List<point>();
		Read("resources/sphere_nodes.txt");
		BuildFEAMatrix ();
	//	TestCollisionDetection ();

	}
	
	
	public fea_second(bool skip,string fileName)
	{
		Elements = new List<element>();
		Points = new List<point>();
		Read(fileName);
		if(!skip)
		{
			BuildFEAMatrix ();
		}
	}

	public static List<element> Elements;
	public static List<point> Points;    
	public static Queue<element> CollideTets;
	public static List<element> CollideTets_ForTest;
	public static double DistanceLimit = 0.3;
	private Queue<element> NeigborAroundCollision;
	private GameObject DeformableObj;
	private static int dimension = 3;
	private Matrix<double> GlobalK;
	
	public class element
	{
		public int label;
		public point[] p = new point[4];
		public List<element> elem = new List<element>();  // the neibor element
		public bool visit;
		
	}
	
	public class point
	{
		public Vector3 pos;
		public int label;
		public List<element>  elements = new List<element>();
	}
	
	void Read(string fileName)
	{
		FileInfo theSourceFile = null;
		StreamReader reader = null;
		string text = " "; 
		Vector3 vertex;
		int[] tetrhedra;
		ArrayList vertices,tetrhedras;
		
		
		theSourceFile = new FileInfo(fileName);

		reader = theSourceFile.OpenText();
		text = reader.ReadLine();
		vertices = new ArrayList ();
		tetrhedras = new ArrayList ();
		while (text != null) 
		{
			string[] arr;
			arr = text.Split(' ');
			vertex = new Vector3(float.Parse(arr[0]),float.Parse(arr[1]),float.Parse(arr[2]));
			vertices.Add(vertex);
			//	Debug.Log(vertex);
			text = reader.ReadLine();//Debug.Log(vertex[0]+" "+vertex[1]+" "+vertex[2]);
		}
		reader.Close ();
	
		theSourceFile = new FileInfo("resources/sphere_elements.txt");
		reader = theSourceFile.OpenText();
		text = reader.ReadLine();
		
		while (text != null) 
		{
			string[] arr;
			arr = text.Split(' ');//Debug.Log(arr);
			tetrhedra = new int[4];
			tetrhedra[0] = int.Parse(arr[0]);
			tetrhedra[1] = int.Parse(arr[1]);
			tetrhedra[2] = int.Parse(arr[2]);
			tetrhedra[3] = int.Parse(arr[3]);
			tetrhedras.Add(tetrhedra);
			text = reader.ReadLine();//Debug.Log(tetrhedra[0]+" "+tetrhedra[1]+" "+tetrhedra[2]+" "+tetrhedra[3]);
		}
		reader.Close ();
		//Debug.Log (tetrhedras.Count);
		
		MeshConstruction (vertices, tetrhedras);
		//	testMYFEA ();
	}
	
	void MeshConstruction(ArrayList vertices,ArrayList tetrhedras)
	{
		int index = 1;
		Vector3[] vertices_array = ConverterClass.ConvertArrayListToVector3 (vertices);
		
		// since the point label start at 1, need to give a garbage
		point p = new point();
		p.label=-1;
		Points.Add(p);
		
		foreach(var item in vertices_array)
		{
			p = new point();
			p.pos = item;
			p.label=index++;
			Points.Add(p);
		}
		
		
		element e = new element();
		e.label = -1;
		Elements.Add(e);
		
		index = 1;
		foreach(int[] item in tetrhedras)
		{
			e = new element();
			for (int i=0;i<item.Length;i++)
				e.p[i]= Points[item[i]]; 
			e.label=index++;
			e.visit=false;
			Elements.Add(e);
		}
		
		FindNeighbor();
		
	}
	
	void FindNeighbor()
	{
		
		foreach(element element_item in Elements.Skip(1))
		{
			int[] element_point = new int[element_item.p.Length];
			for(int i=0;i<element_item.p.Length;i++)
			{
				element_point[i]=element_item.p[i].label;
			}
			
			for(int i=0;i<element_point.Length;i++)
			{
				Points[element_point[i]].elements.Add(element_item);
			}
		}
		
		int index = 1;
		/*	foreach(point Points_element in Points)
		{
			Debug.Log(Points_element.label);
			foreach(element Point_element in Points_element.elements)
			{
				Debug.Log(Point_element.label);
			}
			index++;
		}  */
		
		foreach(element element_item in Elements.Skip(1))
		{
			//Debug.Log("index "+index);
			int[] element_point = new int[element_item.p.Length];
			for(int i=0;i<element_item.p.Length;i++)   // element_point: 1 2 3 4
			{
				element_point[i]=element_item.p[i].label;
			}
			for(int i=0;i<element_point.Length;i++)
			{
				List<element> PointElements = Points[element_point[i]].elements;
				foreach(element PointElement in PointElements)
				{
					int[] p_label = new int[PointElement.p.Length];
					for(int j=0;j<PointElement.p.Length;j++)
						p_label[j] = PointElement.p[j].label;
					HashSet<int> set1 = new HashSet<int> (element_point.OfType<int>().ToList());
					HashSet<int> set2 = new HashSet<int> (p_label.OfType<int>().ToList());
					var equals = set1.Intersect(set2);	
					int CommonNumber = equals.Count();
					if(CommonNumber==3)
					{
						Elements[index].elem.Add(PointElement);
						//	Debug.Log(PointElement.label);
					}
					
				}
			}
			index++;
			
		}
		
		for(int i=0;i<Elements.Count;i++)
		{
			List<element> temp = Elements[i].elem;List<element> m;
			//	temp.Distinct (new Comparer ());
			Elements[i].elem = temp.GroupBy(elem=>elem.label).Select(group=>group.First()).ToList();
			//	foreach(element t in m)
			//Debug.Log(t.label);
		}
		
	}
	

	void ControlVertices(Vector3 needle_start,Vector3 needle_end)
	{
		element TetAddForce = null;
		
		while (CollideTets.Count != 0)
		{
			element temp = CollideTets.Dequeue();
			if (PointInTet(needle_end, temp))
			{
				TetAddForce = temp;
				break;
			}                
		}
		//	Debug.Log("TetAddForce = null");
		if(TetAddForce != null)
		{
			//Debug.Log("TetAddForce != null");
			FEA_Call(TetAddForce,needle_start, needle_end );    
			//			Debug.Log(TetAddForce.p[0].pos+" "+TetAddForce.p[1].pos+" "+TetAddForce.p[2].pos);
			//	Debug.Log(TetAddForce.label);
		}
	}
	
	// http://steve.hollasch.net/cgindex/geometry/ptintet.html
	public static bool PointInTet(Vector3 p,element tet)
	{
		point[] tet_points = tet.p;
		Vector3 p1, p2, p3, p4;
		double x,y,z,x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4,det1,det2,det3,det4;
		Matrix<double> D0, D1, D2, D3, D4;
		x = p[0];
		y = p[1];
		z = p[2];
		p1 = tet_points[0].pos;
		p2 = tet_points[1].pos;
		p3 = tet_points[2].pos;
		p4 = tet_points[3].pos;
		x1 = p1[0];
		x2 = p2[0];
		x3 = p3[0];
		x4 = p4[0];
		y3 = p3[1]; 
		y4 = p4[1]; 
		y1 = p1[1];
		y2 = p2[1]; 
		z1 = p1[2]; 
		z2 = p2[2]; 
		z3 = p3[2]; 
		z4 = p4[2];         
		
		D0 = DenseMatrix.OfArray(new double[,] {
			{x1, y1, z1,1.0},
			{x2, y2, z2,1.0},
			{x3, y3, z3,1.0},
			{x4, y4, z4,1.0}});
		
		D1 = DenseMatrix.OfArray(new double[,] {
			{x , y,  z,  1},
			{x2 ,y2 ,z2, 1},
			{x3, y3 ,z3, 1},
			{x4, y4, z4 ,1}});
		
		D2  =  DenseMatrix.OfArray(new double[,] {
			{x1, y1, z1 ,1},
			{x , y  ,z  ,1},
			{x3, y3, z3 ,1},
			{x4 ,y4 ,z4 ,1}});
		
		D3  =  DenseMatrix.OfArray(new double[,] {
			{x1, y1 ,z1, 1},
			{x2 ,y2 ,z2 ,1},
			{x  ,y,  z  ,1},
			{x4 ,y4, z4 ,1}});
		
		D4  =  DenseMatrix.OfArray(new double[,] {
			{x1, y1 ,z1 ,1},
			{x2 ,y2 ,z2 ,1},
			{x3 ,y3 ,z3 ,1},
			{x  ,y  ,z  ,1}});
		
		det1 = D1.Determinant();
		det2 = D2.Determinant();
		det3 = D3.Determinant();
		det4 = D4.Determinant();
		return (det1 > 0 && det2 > 0 && det3 > 0 && det4 > 0) ||
			(det1 < 0 && det2 < 0 && det3 < 0 && det4 < 0);
	}
	
	public List<element> TetForFEA(element TetAddForce, Vector3 needle_end)
	{
		Debug.Log ("TetForFEA");
		//Debug.Log ("TetAddForce " + TetAddForce.label);
		Vector3 centroid, ProjectToNeedle;
		Queue<element> neighbor = new Queue<element>();
		List<element> Visited = new List<element>();
		Visited.Add (TetAddForce);
		TetAddForce.visit = true;
		neighbor.Enqueue(TetAddForce);
		//	Debug.Log("needle_end "+needle_end);
		//Debug.Log("needle_start "+needle_start);
		Vector3 a = needle_end;
		//Debug.Log ("a - b" + Vector3.Distance (a , b));
		ProjectToNeedle = needle_end;
		while (neighbor.Count > 0)
		{
			element e = neighbor.Dequeue();
			centroid = (e.p[0].pos + e.p[1].pos + e.p[2].pos + e.p[3].pos) / 4;
			//ProjectToNeedle = Math3d.ProjectPointOnLineSegment(needle_start, needle_end, centroid);
			
			if(Vector3.Distance(centroid,needle_end) < DistanceLimit)
			{
				//Debug.Log(" centroid "+centroid);
				foreach (element temp in e.elem)
				{
					if(Vector3.Distance((temp.p[0].pos+temp.p[0].pos+temp.p[0].pos+temp.p[0].pos)/4,needle_end)<DistanceLimit)
					{
						if (!temp.visit )
						{
							Visited.Add(temp);
							temp.visit = true; //
							neighbor.Enqueue(temp);                    
						}
					}
				}
			}
		}
		
		
		foreach (element e in Visited)
		{
			e.visit = false;
			//Debug.Log(e.label);
			int label = e.label-1;
			//	writer.WriteLine(label+",");
			
		}
		//	writer.Close();
		return Visited;
	}
	
	int NumOfUniquePoints(List<element> freePointLabel,out List<int> freeTet)
	{
		HashSet<int> label = new HashSet<int>();
		freeTet = new List<int> (); 
		int index = 0;

		//Debug.Log (Elements[0].p[0].label);
		foreach (element e in freePointLabel)
		{
			label.Add(e.p[0].label);
			label.Add(e.p[1].label);
			label.Add(e.p[2].label);
			label.Add(e.p[3].label);
		}
		freeTet = label.ToList ();//Debug.Log ("freeTet.Count+ "+freeTet.Count);
		return label.Count();
	}

    public int ReturnNumOfUniquePoints(List<element> freePointLabel,out List<int> freeTet)
    {

        return NumOfUniquePoints(freePointLabel,out freeTet);
    }
	
	void FindFixPoints(Dictionary<int, int> label,Vector3 tip,List<int> FixedPointIndex)
	{
		// 1/3 of points are fixed 
		double[] distance= new double[label.Count];
		Dictionary<double, int> distanceToLabel = new Dictionary<double, int>();	
		int NumFixedPoint, end,dist_index;
		NumFixedPoint = label.Count / 3;
		end = NumFixedPoint - 1;
		
		dist_index = 0;
		
		foreach(KeyValuePair<int, int> entry in label)
		{
			// do something with entry.Value or entry.Key
			distance[dist_index]=(Points[entry.Value].pos-tip).magnitude;
			distanceToLabel[distance[dist_index]] = entry.Value;
			dist_index++;
		}
		
		Array.Sort (distance);
		for(int i=0;i<NumFixedPoint;i++)
		{
			//Debug.Log("distanceToLabel[distance[end--]] "+distanceToLabel[distance[end--]]);
			FixedPointIndex.Add(distanceToLabel[distance[end--]]); 
		}
		
	}

	void BuildFEAMatrix()
	{
	//	Debug.Log (dimension * (Elements.Count-1));
		Matrix<double> K = Matrix<double>.Build.Dense(dimension * (Elements.Count-1), dimension * (Elements.Count-1));
		double E, NU, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4;
		E = 101*Mathf.Pow(10,3); // 
		NU = 0.3;
		foreach (element e in Elements)
		{
			if(e.label!=-1)
			{
				x1=e.p[0].pos[0];
				x2=e.p[1].pos[0];
				x3=e.p[2].pos[0];
				x4=e.p[3].pos[0];
				y1=e.p[0].pos[1];
				y2=e.p[1].pos[1];
				y3=e.p[2].pos[1];
				y4=e.p[3].pos[1];
				z1=e.p[0].pos[2];
				z2=e.p[1].pos[2];
				z3=e.p[2].pos[2];
				z4=e.p[3].pos[2];
				
				Matrix<double> k = TetrahedronElementStiffness(E, NU, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4);
				K = TetrahedronAssemble(K, k, e.p[0].label, e.p[1].label, e.p[2].label, e.p[3].label);
				//	K = TetrahedronAssemble(K, k, e.p[0].label, e.p[1].label, e.p[2].label, e.p[3].label);
				
				//Debug.Log(e.p[0].label+" "+ e.p[1].label+" "+e.p[2].label+" "+ e.p[3].label);
				//	Debug.Log(transformedLabel[e.p[0].label]+" "+transformedLabel[e.p[1].label]+" "+transformedLabel[e.p[2].label]+" "+ transformedLabel[e.p[3].label]);
			}
		}

		GlobalK = K;
	}

	public Matrix<double> ReturnGlobalK()
	{
		return GlobalK;
	}

	void FEA_Call(element TetAddForce,Vector3 needle_start, Vector3 needle_end)
	{	
		System.Diagnostics.Stopwatch stopWatch = new System.Diagnostics.Stopwatch();
		stopWatch.Start();
		TimeSpan ts = stopWatch.Elapsed;
		Debug.Log("Start TIme: " + ts.Milliseconds);

		double x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4;
		int Num_uniq;
		Vector<double> BoundaryCondition, FinalPos;
		List<element> Visited = TetForFEA(TetAddForce, needle_end);
		List<int> freeTet = new List<int> ();

		freeTet = new List<int> (); 
		Num_uniq = NumOfUniquePoints (Visited,out freeTet);
		//Debug.Log (Num_uniq);
		if (Num_uniq == 0)
		{Debug.Log("PointNum");
			return;
		}

		//	Debug.Log ("PointNum " + PointNum+" fixedPointLabel.Count "+fixedPointLabel.Count);
		//PickMovingVertex(List<int> freePointLabel,Matrix<double> K);
		Vector3 force = (needle_end-needle_start).normalized*100f;

		Matrix<double> freeMatrix_local = PickMovingVertex(freeTet, GlobalK);
		//Debug.Log (freeMatrix_local);
		Vector<double> forceVector = EnforceBoundary (freeTet, TetAddForce);
	
		ts = stopWatch.Elapsed;
//		Debug.Log("before inverse: " + ts.Milliseconds);

		Vector<double> displacement = freeMatrix_local.Inverse () * forceVector;
	//	Debug.Log (freeMatrix_local.Column());
		ts = stopWatch.Elapsed;
	//	Debug.Log("after inverse: " + ts.Milliseconds);

		SetPointsAfterFEA (displacement, freeTet);
		//Debug.Log (displacement);
		//	MatrixMarketWriter.WriteMatrix("K.mtx", K);
	//	Debug.Log (freeMatrix_local);
	}  

	void SetPointsAfterFEA(Vector<double> displacement,List<int> freeTet)
	{
		int j = 0;
		for(int i=0;i<freeTet.Count;i++)
		{
			Points[freeTet[i]].pos+=new Vector3((float)displacement[j], (float)displacement[j+1], (float)displacement[j+2]);
			CreateMesh.vertices[freeTet[i]]=Points[freeTet[i]].pos;
			j+=3;
		}

		j = 0;
		for(int i=0;i<freeTet.Count;i++)
		{
			Points[freeTet[i]].pos-=new Vector3((float)displacement[j], (float)displacement[j+1], (float)displacement[j+2]);
			j+=3;
		}
	}
	
    public void ReturnSetPointsAfterFEA(Vector<double> displacement,List<int> freeTet)
    {
        SetPointsAfterFEA(displacement,freeTet);
    }

	Vector<double> EnforceBoundary(List<int> freeTet,element TetAddForce)
	{
		List<int> forceIndex = new List<int>();
		Vector<double> PickBoundary = Vector<double>.Build.Dense(3 * freeTet.Count);
		for(int i=0;i<4;i++)
		{
			int index = TetAddForce.p[i].label;
			forceIndex.Add(freeTet.IndexOf(index));
		}

		for(int i=0;i<forceIndex.Count;i++)
		{
			PickBoundary[forceIndex[i]] = 500;
		}


		return PickBoundary;
	}
	
    public Vector<double> ReturnEnforceBoundary(List<int> freeTet,element TetAddForce)
    {
        return EnforceBoundary(freeTet, TetAddForce);
    }

	List<int> FindfreePointLabel(Dictionary<int, int> transformedLabel,List<int> fixedPointLabel,List<int> allLabel)
	{
		List<int> freePointLabel;
		freePointLabel = GenericCopier<List<int>>.DeepCopy(allLabel);
		//	Debug.Log ("freePointLabel before " + freePointLabel.Count);
		foreach(int label in fixedPointLabel)
		{
			freePointLabel.Remove(transformedLabel[label]);
		}
		//		Debug.Log ("freePointLabel after " + freePointLabel.Count);
		return freePointLabel;
	}
	
	public Matrix<double> PickMovingVertex(List<int> freePointLabel,Matrix<double> K)
	{
	//	Debug.Log ("freePointLabel.Count "+freePointLabel.Count);
		Matrix<double> freeMatrix = Matrix<double>.Build.Dense(3 * freePointLabel.Count, 3 * freePointLabel.Count);
		List<int> begin_labe,end_label;
		
		begin_labe = new List<int> ();
		end_label = new List<int> ();
		freePointLabel.Sort ();
		//Debug.Log (3 * freePointLabel.Count);
		foreach(int label in freePointLabel)
		{
			int begin, end;
			begin = (label-1)*3;
			end = (label-1)*3+2;
			//	Debug.Log(begin);
			begin_labe.Add(begin);
			end_label.Add(end);
		}
		
		int[] r1 = new int[2];
		int[] r2 = new int[2];
		
		for(int i=0;i<begin_labe.Count;i++)
		{
			//	Debug.Log("begin_labe "+begin_labe[i]);
			r1[0] = begin_labe[i];
			r1[1] = end_label[i];
			for(int j=0;j<end_label.Count;j++)
			{
				r2[0] = begin_labe[j];
				r2[1] = end_label[j];
				for(int k=r1[0];k<=r1[1];k++)
				{
					for(int m=r2[0];m<=r2[1];m++)
					{
						freeMatrix[i*dimension+k-r1[0],j*dimension+m-r2[0]] = K[k,m];
					}
				}
			}
		}
		//Debug.Log (freeMatrix);
		return freeMatrix;
	}

	void DeleteElement()
	{
		while(CollideTets.Count>0)
		{
			int index = CollideTets.Dequeue().label;
			int[] PointIndex = {Elements[index].p[0].label,Elements[index].p[1].label, Elements[index].p[2].label,Elements[index].p[3].label};
			for (int i = 0; i < CreateMesh.faces.Count;i+=3)
			{
				int[] temp = { CreateMesh.faces[i], CreateMesh.faces[i + 1], CreateMesh.faces[i + 2] };
				int[] f1 = {PointIndex[0],PointIndex[1],PointIndex[2]};
				int[] f2 = {PointIndex[0],PointIndex[1],PointIndex[3]};
				int[] f3 = {PointIndex[0],PointIndex[2],PointIndex[3]};
				int[] f4 = {PointIndex[1],PointIndex[2],PointIndex[3]};
				if(ArrayExtensions.equal(temp,f1) || ArrayExtensions.equal(temp, f2) || ArrayExtensions.equal(temp, f3) || ArrayExtensions.equal(temp, f4))
				{
					CreateMesh.faces.RemoveAt(i); CreateMesh.faces.RemoveAt(i+1); CreateMesh.faces.RemoveAt(i+2);
					break;
				}              
			}
			
			// all collide elements are removed
			Elements.RemoveAt(index); 
			
		}
	}
	

	/*  List<element> TetrahedralAffected()
    {
        Queue<element> current = new Queue<element>(CollideTets);
        List<element> AffectedTet;
        Vector3 ProjectToNeedle;
        while(current.Count>0)
        {
            element e = current.Dequeue();
            centroid = (e.p[0].pos + e.p[1].pos + e.p[2].pos + e.p[3].pos) / 4;
            ProjectToNeedle = Math3d.ProjectPointOnLineSegment(needle_start, needle_end, centroid);
            if (Vector3.Distance(centroid, ProjectToNeedle) <= DistanceLimit)
            {
                foreach (element neighbor in e.elem)
                {
                    if (!neighbor.visit)
                    {
                        Visited.Add(neighbor);
                        neighbor.visit = true;
                        if (LineIntersectTetrahedron(neighbor, needle_start, needle_end))
                        {
                            AffectedTet.Enqueue(neighbor);
                            temp_collide.Enqueue(neighbor);
                        }
                    }
                }
            }            
        }
    }
    */
	public Matrix<double> TetrahedronElementStiffness(double E, double NU, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4)
	{
		double V,beta1,beta2,beta3,beta4,gamma1,gamma2,gamma3,gamma4,delta1,delta2,delta3,delta4;
		Matrix<double> xyz,mbeta1,mbeta2,mbeta3,mbeta4,mgamma1,mgamma2,mgamma3,mgamma4,mdelta1,mdelta2,mdelta3,mdelta4,B,B1,B2,B3,B4,D,y;
		
		xyz = DenseMatrix.OfArray(new double[,] {
			{1.0, x1, y1, z1},
			{1, x2, y2, z2},
			{1.0, x3, y3, z3},
			{1.0, x4, y4, z4}
		});
		V = xyz.Determinant ()/6;
		mbeta1=DenseMatrix.OfArray(new double[,] {
			{1, y2, z2},
			{1, y3, z3},
			{1, y4, z4}});
		mbeta2=DenseMatrix.OfArray(new double[,] {
			{1, y1, z1},
			{1, y3, z3},
			{1, y4, z4}});
		mbeta3=DenseMatrix.OfArray(new double[,] {
			{1, y1, z1},
			{1, y2, z2},
			{1, y4, z4}});
		mbeta4=DenseMatrix.OfArray(new double[,] {
			{1, y1, z1},
			{1, y2, z2},
			{1, y3, z3}});
		
		mgamma1=DenseMatrix.OfArray(new double[,] {
			{1, x2, z2},
			{1, x3, z3},
			{1, x4, z4}});
		mgamma2=DenseMatrix.OfArray(new double[,] {
			{1, x1, z1},
			{1, x3, z3},
			{1, x4, z4}});
		mgamma3=DenseMatrix.OfArray(new double[,] {
			{1, x1, z1},
			{1, x2, z2},
			{1, x4, z4}});
		mgamma4=DenseMatrix.OfArray(new double[,] {
			{1, x1, z1},
			{1, x2, z2},
			{1, x3, z3}});
		
		mdelta1=DenseMatrix.OfArray(new double[,] {
			{1, x2, y2},
			{1, x3, y3},
			{1, x4, y4}});
		mdelta2=DenseMatrix.OfArray(new double[,] {
			{1, x1, y1},
			{1, x3, y3},
			{1, x4, y4}});
		mdelta3=DenseMatrix.OfArray(new double[,] {
			{1, x1, y1},
			{1, x2, y2},
			{1, x4, y4}});
		mdelta4=DenseMatrix.OfArray(new double[,] {
			{1, x1, y1},
			{1, x2, y2},
			{1, x3, y3}});
		
		beta1 = -1*mbeta1.Determinant();
		beta2 = mbeta2.Determinant();
		beta3 = -1*mbeta3.Determinant();
		beta4 = mbeta4.Determinant();
		gamma1 = mgamma1.Determinant();
		gamma2 = -1*mgamma2.Determinant();
		gamma3 = mgamma3.Determinant();
		gamma4 = -1*mgamma4.Determinant();
		delta1 = -1*mdelta1.Determinant();
		delta2 = mdelta2.Determinant();
		delta3 = -1*mdelta3.Determinant();
		delta4 = mdelta4.Determinant();	
		B1 = DenseMatrix.OfArray (new double[,] {
			{beta1, 0, 0}, {0, gamma1, 0},{0, 0, delta1}, 
			{gamma1, beta1, 0},{0, delta1, gamma1}, {delta1, 0, beta1}
		});
		
		
		B2 = DenseMatrix.OfArray (new double[,] {
			{beta2,0, 0},{0, gamma2, 0},{0, 0, delta2}, 
			{gamma2, beta2, 0},{0, delta2, gamma2}, {delta2, 0, beta2}
		});
		
		
		B3=DenseMatrix.OfArray(new double[,] {
			{beta3, 0, 0},{0, gamma3, 0},{ 0, 0, delta3}, 
			{gamma3, beta3, 0} , {0, delta3, gamma3} , {delta3, 0, beta3}
		});
		
		
		B4 = DenseMatrix.OfArray (new double[,] {
			{beta4, 0, 0}, {0, gamma4, 0},{ 0, 0, delta4}, 
			{gamma4, beta4, 0} ,{0, delta4, gamma4},{ delta4, 0, beta4}
		});
		
		B = B1.Append(B2);
		B = B.Append(B3);
		B = B.Append(B4);
		B = B/(6*V);
		
		D = DenseMatrix.OfArray (new double[,] {
			{1-NU, NU, NU, 0, 0, 0}, {NU, 1-NU, NU, 0, 0, 0}, {NU, NU, 1-NU, 0, 0, 0},
			{ 0, 0, 0, (1-2*NU)/2, 0 ,0},{ 0, 0, 0, 0, (1-2*NU)/2, 0}, {0, 0, 0, 0, 0, (1-2*NU)/2}
		});
		
		D = (E/((1+NU)*(1-2*NU)))*D;
		y = V*B.Transpose()*D*B;
		return y;
	}
	
	static double TetrahedronElementVolume(double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4)
	{
		Matrix<double> xyz;
		double V;
		xyz = DenseMatrix.OfArray(new double[,] {
			{1.0, x1, y1, z1},
			{1, x2, y2, z2},
			{1.0, x3, y3, z3},
			{1.0, x4, y4, z4}});
		V = xyz.Determinant() / 6;
		return V;
	}
	
    public double ReturnTetrahedronElementVolume(List<double[]> p)
    {
        double[] p0=p[0],p1=p[1],p2=p[2],p3=p[3];
        return TetrahedronElementVolume(p0[0], p0[1], p0[2], p1[0], p1[1], p1[2], p2[0], p2[1], p2[2], p3[0], p3[1], p3[2]);
    }

	public static double ConvertIndexFromMatlab(Matrix<double> k,int i,int j)
	{
		return k[i-1,j-1];
	}
	
//  public ReturnConvertIndexFromMatlab() 

	public Matrix<double> TetrahedronAssemble(Matrix<double> K, Matrix<double> k, int i, int j, int m, int n)
	{
		
		K[3*i-2-1,3*i-2-1] = ConvertIndexFromMatlab(K,3*i-2,3*i-2) + ConvertIndexFromMatlab(k,1,1);
		K[3*i-2-1,3*i-1-1] = ConvertIndexFromMatlab(K,3*i-2,3*i-1) + ConvertIndexFromMatlab(k,1,2);
		K[3*i-2-1,3*i-1] = ConvertIndexFromMatlab(K,3*i-2,3*i) + ConvertIndexFromMatlab(k,1,3);
		K[3*i-2-1,3*j-2-1] = ConvertIndexFromMatlab(K,3*i-2,3*j-2) + ConvertIndexFromMatlab(k,1,4);
		K[3*i-2-1,3*j-1-1] = ConvertIndexFromMatlab(K,3*i-2,3*j-1) +  ConvertIndexFromMatlab(k,1,5);
		K[3*i-2-1,3*j-1] = ConvertIndexFromMatlab(K,3*i-2,3*j) + ConvertIndexFromMatlab(k,1,6);;
		K[3*i-2-1,3*m-2-1] = ConvertIndexFromMatlab(K,3*i-2,3*m-2) + ConvertIndexFromMatlab(k,1,7);
		K[3*i-2-1,3*m-1-1] = ConvertIndexFromMatlab(K,3*i-2,3*m-1) + ConvertIndexFromMatlab(k,1,8);
		K[3*i-2-1,3*m-1] = ConvertIndexFromMatlab(K,3*i-2,3*m) + ConvertIndexFromMatlab(k,1,9);
		K[3*i-2-1,3*n-2-1] = ConvertIndexFromMatlab(K,3*i-2,3*n-2) + ConvertIndexFromMatlab(k,1,10);
		K[3*i-2-1,3*n-1-1] = ConvertIndexFromMatlab(K,3*i-2,3*n-1) +  ConvertIndexFromMatlab(k,1,11);
		K[3*i-2-1,3*n-1] = ConvertIndexFromMatlab(K,3*i-2,3*n) + ConvertIndexFromMatlab(k,1,12);
		K[3*i-1-1,3*i-2-1] = ConvertIndexFromMatlab(K,3*i-1,3*i-2) + ConvertIndexFromMatlab(k,2,1);
		K[3*i-1-1,3*i-1-1] = ConvertIndexFromMatlab(K,3*i-1,3*i-1) + ConvertIndexFromMatlab(k,2,2);
		K[3*i-1-1,3*i-1] = ConvertIndexFromMatlab(K,3*i-1,3*i) + ConvertIndexFromMatlab(k,2,3);
		K[3*i-1-1,3*j-2-1] = ConvertIndexFromMatlab(K,3*i-1,3*j-2)+  ConvertIndexFromMatlab(k,2,4);
		K[3*i-1-1,3*j-1-1] = ConvertIndexFromMatlab(K,3*i-1,3*j-1)+ ConvertIndexFromMatlab(k,2,5);
		K[3*i-1-1,3*j-1] = ConvertIndexFromMatlab(K,3*i-1,3*j)+ ConvertIndexFromMatlab(k,2,6);
		K[3*i-1-1,3*m-2-1] = ConvertIndexFromMatlab(K,3*i-1,3*m-2)+ ConvertIndexFromMatlab(k,2,7);
		K[3*i-1-1,3*m-1-1] = ConvertIndexFromMatlab(K,3*i-1,3*m-1)+ ConvertIndexFromMatlab(k,2,8);
		K[3*i-1-1,3*m-1] = ConvertIndexFromMatlab(K,3*i-1,3*m)+ ConvertIndexFromMatlab(k,2,9);
		K[3*i-1-1,3*n-2-1] = ConvertIndexFromMatlab(K,3*i-1,3*n-2)+ ConvertIndexFromMatlab(k,2,10);
		K[3*i-1-1,3*n-1-1] = ConvertIndexFromMatlab(K,3*i-1,3*n-1)+  ConvertIndexFromMatlab(k,2,11);
		K[3*i-1-1,3*n-1] = ConvertIndexFromMatlab(K,3*i-1,3*n)+ ConvertIndexFromMatlab(k,2,12);
		K[3*i-1,3*i-2-1] = ConvertIndexFromMatlab(K,3*i,3*i-2)+ ConvertIndexFromMatlab(k,3,1);
		K[3*i-1,3*i-1-1] = ConvertIndexFromMatlab(K,3*i,3*i-1)+ ConvertIndexFromMatlab(k,3,2);
		K[3*i-1,3*i-1] = ConvertIndexFromMatlab(K,3*i,3*i)+ ConvertIndexFromMatlab(k,3,3);
		K[3*i-1,3*j-2-1] = ConvertIndexFromMatlab(K,3*i,3*j-2)+ ConvertIndexFromMatlab(k,3,4);
		K[3*i-1,3*j-1-1] = ConvertIndexFromMatlab(K,3*i,3*j-1)+ ConvertIndexFromMatlab(k,3,5);
		K[3*i-1,3*j-1] = ConvertIndexFromMatlab(K,3*i,3*j)+ ConvertIndexFromMatlab(k,3,6);
		K[3*i-1,3*m-2-1] = ConvertIndexFromMatlab(K,3*i,3*m-2)+ ConvertIndexFromMatlab(k,3,7);
		K[3*i-1,3*m-1-1] = ConvertIndexFromMatlab(K,3*i,3*m-1)+  ConvertIndexFromMatlab(k,3,8);
		K[3*i-1,3*m-1] = ConvertIndexFromMatlab(K,3*i,3*m)+ ConvertIndexFromMatlab(k,3,9);
		K[3*i-1,3*n-2-1] = ConvertIndexFromMatlab(K,3*i,3*n-2)+ ConvertIndexFromMatlab(k,3,10);
		K[3*i-1,3*n-1-1] = ConvertIndexFromMatlab(K,3*i,3*n-1)+ ConvertIndexFromMatlab(k,3, 11);
		K[3*i-1,3*n-1] = ConvertIndexFromMatlab(K,3*i,3*n)+ ConvertIndexFromMatlab(k,3, 12);
		K[3*j-2-1,3*i-2-1] = ConvertIndexFromMatlab(K,3*j-2,3*i-2)+ ConvertIndexFromMatlab(k,4, 1);
		K[3*j-2-1,3*i-1-1] = ConvertIndexFromMatlab(K,3*j-2,3*i-1)+ ConvertIndexFromMatlab(k,4, 2);
		K[3*j-2-1,3*i-1] = ConvertIndexFromMatlab(K,3*j-2,3*i)+  ConvertIndexFromMatlab(k,4, 3);
		K[3*j-2-1,3*j-2-1] = ConvertIndexFromMatlab(K,3*j-2,3*j-2)+ ConvertIndexFromMatlab(k,4, 4);
		K[3*j-2-1,3*j-1-1] = ConvertIndexFromMatlab(K,3*j-2,3*j-1)+ ConvertIndexFromMatlab(k,4, 5);
		K[3*j-2-1,3*j-1] = ConvertIndexFromMatlab(K,3*j-2,3*j)+ ConvertIndexFromMatlab(k,4, 6);
		K[3*j-2-1,3*m-2-1] = ConvertIndexFromMatlab(K,3*j-2,3*m-2)+ ConvertIndexFromMatlab(k,4, 7);
		K[3*j-2-1,3*m-1-1] = ConvertIndexFromMatlab(K,3*j-2,3*m-1)+ ConvertIndexFromMatlab(k,4, 8);
		K[3*j-2-1,3*m-1] = ConvertIndexFromMatlab(K,3*j-2,3*m)+  ConvertIndexFromMatlab(k,4, 9);
		K[3*j-2-1,3*n-2-1] = ConvertIndexFromMatlab(K,3*j-2,3*n-2)+ ConvertIndexFromMatlab(k,4, 10);
		K[3*j-2-1,3*n-1-1] = ConvertIndexFromMatlab(K,3*j-2,3*n-1)+ ConvertIndexFromMatlab(k,4, 11);
		K[3*j-2-1,3*n-1] = ConvertIndexFromMatlab(K,3*j-2,3*n)+ ConvertIndexFromMatlab(k,4, 12);
		K[3*j-1-1,3*i-2-1] = ConvertIndexFromMatlab(K,3*j-1,3*i-2)+ ConvertIndexFromMatlab(k,5, 1);
		K[3*j-1-1,3*i-1-1] = ConvertIndexFromMatlab(K,3*j-1,3*i-1)+ ConvertIndexFromMatlab(k,5, 2);
		K[3*j-1-1,3*i-1] = ConvertIndexFromMatlab(K,3*j-1,3*i)+ ConvertIndexFromMatlab(k,5, 3);
		K[3*j-1-1,3*j-2-1] = ConvertIndexFromMatlab(K,3*j-1,3*j-2)+ ConvertIndexFromMatlab(k,5, 4);
		K[3*j-1-1,3*j-1-1] = ConvertIndexFromMatlab(K,3*j-1,3*j-1)+ ConvertIndexFromMatlab(k,5, 5);
		K[3*j-1-1,3*j-1] = ConvertIndexFromMatlab(K,3*j-1,3*j)+ ConvertIndexFromMatlab(k,5, 6);
		K[3*j-1-1,3*m-2-1] = ConvertIndexFromMatlab(K,3*j-1,3*m-2)+ ConvertIndexFromMatlab(k,5, 7);
		K[3*j-1-1,3*m-1-1] = ConvertIndexFromMatlab(K,3*j-1,3*m-1)+ ConvertIndexFromMatlab(k,5, 8);
		K[3*j-1-1,3*m-1] = ConvertIndexFromMatlab(K,3*j-1,3*m)+ ConvertIndexFromMatlab(k,5, 9);
		K[3*j-1-1,3*n-2-1] = ConvertIndexFromMatlab(K,3*j-1,3*n-2)+ ConvertIndexFromMatlab(k,5, 10);
		K[3*j-1-1,3*n-1-1] = ConvertIndexFromMatlab(K,3*j-1,3*n-1)+ ConvertIndexFromMatlab(k,5, 11);
		K[3*j-1-1,3*n-1] = ConvertIndexFromMatlab(K,3*j-1,3*n)+  ConvertIndexFromMatlab(k,5, 12);
		K[3*j-1,3*i-2-1] = ConvertIndexFromMatlab(K,3*j,3*i-2)+ ConvertIndexFromMatlab(k,6, 1);
		K[3*j-1,3*i-1-1] = ConvertIndexFromMatlab(K,3*j,3*i-1)+ ConvertIndexFromMatlab(k,6, 2);
		K[3*j-1,3*i-1] = ConvertIndexFromMatlab(K,3*j,3*i)+ ConvertIndexFromMatlab(k,6, 3);
		K[3*j-1,3*j-2-1] = ConvertIndexFromMatlab(K,3*j,3*j-2)+  ConvertIndexFromMatlab(k,6, 4);
		K[3*j-1,3*j-1-1] = ConvertIndexFromMatlab(K,3*j,3*j-1)+ConvertIndexFromMatlab(k,6, 5);
		K[3*j-1,3*j-1] = ConvertIndexFromMatlab(K,3*j,3*j)+ ConvertIndexFromMatlab(k,6, 6);
		K[3*j-1,3*m-2-1] = ConvertIndexFromMatlab(K,3*j,3*m-2)+ ConvertIndexFromMatlab(k,6, 7);
		K[3*j-1,3*m-1-1] = ConvertIndexFromMatlab(K,3*j,3*m-1)+ ConvertIndexFromMatlab(k,6, 8);
		K[3*j-1,3*m-1] = ConvertIndexFromMatlab(K,3*j,3*m)+ ConvertIndexFromMatlab(k,6, 9);
		K[3*j-1,3*n-2-1] = ConvertIndexFromMatlab(K,3*j,3*n-2)+ ConvertIndexFromMatlab(k,6, 10);
		K[3*j-1,3*n-1-1] = ConvertIndexFromMatlab(K,3*j,3*n-1)+  ConvertIndexFromMatlab(k,6, 11);
		K[3*j-1,3*n-1] = ConvertIndexFromMatlab(K,3*j,3*n)+ ConvertIndexFromMatlab(k,6, 12);
		K[3*m-2-1,3*i-2-1] = ConvertIndexFromMatlab(K,3*m-2,3*i-2)+ ConvertIndexFromMatlab(k,7, 1);
		K[3*m-2-1,3*i-1-1] = ConvertIndexFromMatlab(K,3*m-2,3*i-1)+ ConvertIndexFromMatlab(k,7, 2);
		K[3*m-2-1,3*i-1] = ConvertIndexFromMatlab(K,3*m-2,3*i)+ ConvertIndexFromMatlab(k,7, 3);
		K[3*m-2-1,3*j-2-1] = ConvertIndexFromMatlab(K,3*m-2,3*j-2)+ ConvertIndexFromMatlab(k,7, 4);
		K[3*m-2-1,3*j-1-1] = ConvertIndexFromMatlab(K,3*m-2,3*j-1)+ ConvertIndexFromMatlab(k,7, 5);
		K[3*m-2-1,3*j-1] = ConvertIndexFromMatlab(K,3*m-2,3*j)+ ConvertIndexFromMatlab(k,7, 6);
		K[3*m-2-1,3*m-2-1] = ConvertIndexFromMatlab(K,3*m-2,3*m-2)+  ConvertIndexFromMatlab(k,7, 7);
		K[3*m-2-1,3*m-1-1] = ConvertIndexFromMatlab(K,3*m-2,3*m-1)+  ConvertIndexFromMatlab(k,7, 8);
		K[3*m-2-1,3*m-1] = ConvertIndexFromMatlab(K,3*m-2,3*m)+ ConvertIndexFromMatlab(k,7, 9);
		K[3*m-2-1,3*n-2-1] = ConvertIndexFromMatlab(K,3*m-2,3*n-2)+ ConvertIndexFromMatlab(k,7, 10);
		K[3*m-2-1,3*n-1-1] = ConvertIndexFromMatlab(K,3*m-2,3*n-1)+ ConvertIndexFromMatlab(k,7, 11);
		K[3*m-2-1,3*n-1] = ConvertIndexFromMatlab(K,3*m-2,3*n)+ ConvertIndexFromMatlab(k,7, 12);
		K[3*m-1-1,3*i-2-1] = ConvertIndexFromMatlab(K,3*m-1,3*i-2)+ConvertIndexFromMatlab(k,8, 1);
		K[3*m-1-1,3*i-1-1] = ConvertIndexFromMatlab(K,3*m-1,3*i-1)+ ConvertIndexFromMatlab(k,8, 2);
		K[3*m-1-1,3*i-1] = ConvertIndexFromMatlab(K,3*m-1,3*i)+ ConvertIndexFromMatlab(k,8, 3);
		K[3*m-1-1,3*j-2-1] = ConvertIndexFromMatlab(K,3*m-1,3*j-2)+ ConvertIndexFromMatlab(k,8, 4);
		K[3*m-1-1,3*j-1-1] = ConvertIndexFromMatlab(K,3*m-1,3*j-1)+ ConvertIndexFromMatlab(k,8, 5);
		K[3*m-1-1,3*j-1] = ConvertIndexFromMatlab(K,3*m-1,3*j)+ ConvertIndexFromMatlab(k,8, 6);
		K[3*m-1-1,3*m-2-1] = ConvertIndexFromMatlab(K,3*m-1,3*m-2)+ ConvertIndexFromMatlab(k,8, 7);
		K[3*m-1-1,3*m-1-1] = ConvertIndexFromMatlab(K,3*m-1,3*m-1)+ ConvertIndexFromMatlab(k,8, 8);
		K[3*m-1-1,3*m-1] = ConvertIndexFromMatlab(K,3*m-1,3*m)+ ConvertIndexFromMatlab(k,8, 9);
		K[3*m-1-1,3*n-2-1] = ConvertIndexFromMatlab(K,3*m-1,3*n-2)+ ConvertIndexFromMatlab(k,8, 10);
		K[3*m-1-1,3*n-1-1] = ConvertIndexFromMatlab(K,3*m-1,3*n-1)+ ConvertIndexFromMatlab(k,8, 11);
		K[3*m-1-1,3*n-1] = ConvertIndexFromMatlab(K,3*m-1,3*n)+ ConvertIndexFromMatlab(k,8, 12);
		K[3*m-1,3*i-2-1] = ConvertIndexFromMatlab(K,3*m,3*i-2)+ ConvertIndexFromMatlab(k,9, 1);
		K[3*m-1,3*i-1-1] = ConvertIndexFromMatlab(K,3*m,3*i-1)+ ConvertIndexFromMatlab(k,9, 2);
		K[3*m-1,3*i-1] = ConvertIndexFromMatlab(K,3*m,3*i)+ ConvertIndexFromMatlab(k,9, 3);
		K[3*m-1,3*j-2-1] = ConvertIndexFromMatlab(K,3*m,3*j-2)+ ConvertIndexFromMatlab(k,9, 4);
		K[3*m-1,3*j-1-1] = ConvertIndexFromMatlab(K,3*m,3*j-1)+ ConvertIndexFromMatlab(k,9, 5);
		K[3*m-1,3*j-1] = ConvertIndexFromMatlab(K,3*m,3*j)+ ConvertIndexFromMatlab(k,9, 6);
		K[3*m-1,3*m-2-1] = ConvertIndexFromMatlab(K,3*m,3*m-2)+  ConvertIndexFromMatlab(k,9, 7);
		K[3*m-1,3*m-1-1] = ConvertIndexFromMatlab(K,3*m,3*m-1)+  ConvertIndexFromMatlab(k,9, 8);
		K[3*m-1,3*m-1] = ConvertIndexFromMatlab(K,3*m,3*m)+ ConvertIndexFromMatlab(k,9, 9);
		K[3*m-1,3*n-2-1] = ConvertIndexFromMatlab(K,3*m,3*n-2)+ ConvertIndexFromMatlab(k,9, 10);
		K[3*m-1,3*n-1-1] = ConvertIndexFromMatlab(K,3*m,3*n-1)+  ConvertIndexFromMatlab(k,9, 11);
		K[3*m-1,3*n-1] = ConvertIndexFromMatlab(K,3*m,3*n)+ ConvertIndexFromMatlab(k,9, 12);
		K[3*n-2-1,3*i-2-1] = ConvertIndexFromMatlab(K,3*n-2,3*i-2)+ ConvertIndexFromMatlab(k,10, 1);
		K[3*n-2-1,3*i-1-1] = ConvertIndexFromMatlab(K,3*n-2,3*i-1)+ ConvertIndexFromMatlab(k,10, 2);
		K[3*n-2-1,3*i-1] = ConvertIndexFromMatlab(K,3*n-2,3*i)+  ConvertIndexFromMatlab(k,10, 3);
		K[3*n-2-1,3*j-2-1] = ConvertIndexFromMatlab(K,3*n-2,3*j-2)+ ConvertIndexFromMatlab(k,10, 4);
		K[3*n-2-1,3*j-1-1] = ConvertIndexFromMatlab(K,3*n-2,3*j-1)+ ConvertIndexFromMatlab(k,10, 5);
		K[3*n-2-1,3*j-1] = ConvertIndexFromMatlab(K,3*n-2,3*j)+  ConvertIndexFromMatlab(k,10, 6);
		K[3*n-2-1,3*m-2-1] = ConvertIndexFromMatlab(K,3*n-2,3*m-2)+ConvertIndexFromMatlab(k,10, 7);
		K[3*n-2-1,3*m-1-1] = ConvertIndexFromMatlab(K,3*n-2,3*m-1)+  ConvertIndexFromMatlab(k,10, 8);
		K[3*n-2-1,3*m-1] = ConvertIndexFromMatlab(K,3*n-2,3*m)+ ConvertIndexFromMatlab(k,10, 9);
		K[3*n-2-1,3*n-2-1] = ConvertIndexFromMatlab(K,3*n-2,3*n-2)+ ConvertIndexFromMatlab(k,10, 10);
		K[3*n-2-1,3*n-1-1] = ConvertIndexFromMatlab(K,3*n-2,3*n-1)+ ConvertIndexFromMatlab(k,10, 11);
		K[3*n-2-1,3*n-1] = ConvertIndexFromMatlab(K,3*n-2,3*n)+ ConvertIndexFromMatlab(k,10, 12);
		K[3*n-1-1,3*i-2-1] = ConvertIndexFromMatlab(K,3*n-1,3*i-2)+ ConvertIndexFromMatlab(k,11, 1);
		K[3*n-1-1,3*i-1-1] = ConvertIndexFromMatlab(K,3*n-1,3*i-1)+  ConvertIndexFromMatlab(k,11, 2);
		K[3*n-1-1,3*i-1] = ConvertIndexFromMatlab(K,3*n-1,3*i)+ ConvertIndexFromMatlab(k,11, 3);
		K[3*n-1-1,3*j-2-1] = ConvertIndexFromMatlab(K,3*n-1,3*j-2)+ ConvertIndexFromMatlab(k,11, 4);
		K[3*n-1-1,3*j-1-1] = ConvertIndexFromMatlab(K,3*n-1,3*j-1)+ ConvertIndexFromMatlab(k,11, 5);
		K[3*n-1-1,3*j-1] = ConvertIndexFromMatlab(K,3*n-1,3*j)+ ConvertIndexFromMatlab(k,11, 6);
		K[3*n-1-1,3*m-2-1] = ConvertIndexFromMatlab(K,3*n-1,3*m-2)+ ConvertIndexFromMatlab(k,11, 7);
		K[3*n-1-1,3*m-1-1] = ConvertIndexFromMatlab(K,3*n-1,3*m-1)+ ConvertIndexFromMatlab(k,11, 8);
		K[3*n-1-1,3*m-1] = ConvertIndexFromMatlab(K,3*n-1,3*m)+  ConvertIndexFromMatlab(k,11, 9);
		K[3*n-1-1,3*n-2-1] = ConvertIndexFromMatlab(K,3*n-1,3*n-2)+ ConvertIndexFromMatlab(k,11, 10);
		K[3*n-1-1,3*n-1-1] = ConvertIndexFromMatlab(K,3*n-1,3*n-1)+ ConvertIndexFromMatlab(k,11, 11);
		K[3*n-1-1,3*n-1] = ConvertIndexFromMatlab(K,3*n-1,3*n)+  ConvertIndexFromMatlab(k,11, 12);
		K[3*n-1,3*i-2-1] = ConvertIndexFromMatlab(K,3*n,3*i-2)+ ConvertIndexFromMatlab(k,12, 1);
		K[3*n-1,3*i-1-1] = ConvertIndexFromMatlab(K,3*n,3*i-1)+ ConvertIndexFromMatlab(k,12, 2);
		K[3*n-1,3*i-1] = ConvertIndexFromMatlab(K,3*n,3*i)+ ConvertIndexFromMatlab(k,12, 3);
		K[3*n-1,3*j-2-1] = ConvertIndexFromMatlab(K,3*n,3*j-2)+ ConvertIndexFromMatlab(k,12, 4);
		K[3*n-1,3*j-1-1] = ConvertIndexFromMatlab(K,3*n,3*j-1)+ ConvertIndexFromMatlab(k,12, 5);
		K[3*n-1,3*j-1] = ConvertIndexFromMatlab(K,3*n,3*j)+ ConvertIndexFromMatlab(k,12, 6);
		K[3*n-1,3*m-2-1] = ConvertIndexFromMatlab(K,3*n,3*m-2)+ ConvertIndexFromMatlab(k,12, 7);
		K[3*n-1,3*m-1-1] = ConvertIndexFromMatlab(K,3*n,3*m-1)+ ConvertIndexFromMatlab(k,12, 8);
		K[3*n-1,3*m-1] = ConvertIndexFromMatlab(K,3*n,3*m)+ ConvertIndexFromMatlab(k,12, 9);
		K[3*n-1,3*n-2-1] = ConvertIndexFromMatlab(K,3*n,3*n-2)+ ConvertIndexFromMatlab(k,12, 10);
		K[3*n-1,3*n-1-1] = ConvertIndexFromMatlab(K,3*n,3*n-1)+  ConvertIndexFromMatlab(k,12, 11);
		K[3*n-1,3*n-1] = ConvertIndexFromMatlab(K,3*n,3*n)+ ConvertIndexFromMatlab(k,12, 12);		
		return K;
		
	}
	
	public void CollisionDetection(Vector3 needle_start,Vector3 needle_end)
	{
		
		DeformableObj = GameObject.Find("cube_tet_surface");
		if (CollideTets == null)
		{
			CollideTets = new Queue<element>();
			CollideTets_ForTest = new List<element>();
		}
		if (CollideTets.Count>0)
		{
		
		     //DeleteElement();
			//Debug.Log("haha");
		    CheckNeiborTet(needle_start,needle_end);            
		} 
		else 
		{
			foreach(element e in Elements.Skip(1))
			{
				LineIntersectTetrahedron(e, needle_start, needle_end);

			}
		}
		//	Debug.Log(CollideTets.Count);
		if (CollideTets.Count > 0)
		{//Debug.Log("haha");
			ControlVertices(needle_start,needle_end);             
		}
		
		// Debug.Log(CollideTets.Count);
	}
	
	// this is used by DeleteElem class
	private bool checkNeiborForSingleTet(Vector3 needle_start,Vector3 needle_end,element e)
	{
		int index = 0;
		while(index<e.elem.Count)
		{
			element temp_e = e.elem[index];
			if(LineIntersectTetrahedron(temp_e, needle_start, needle_end))
			{
				CollideTets.Enqueue(temp_e);
				return true;
			}
			index++;
		}
		
		return false;
	}

	public bool ReturnCheckNeiborForSingleTet(Vector3 needle_start,Vector3 needle_end,element e)
	{
		return checkNeiborForSingleTet (needle_start, needle_end, e);
	}
	
	public bool Collision(Vector3 needle_start,Vector3 needle_end)
	{
		
		DeformableObj = GameObject.Find("cube_tet_surface");
		if (CollideTets == null)
			CollideTets = new Queue<element>();
		
		if (CollideTets.Count>0)
		{	
			//Debug.Log("use neighbor info");
			CheckNeiborTet(needle_start,needle_end);
			DeleteElement();
			
		} 
		else 
		{
			//Debug.Log("not use neighbor info");
			foreach(element e in Elements.Skip(1))
			{
				LineIntersectTetrahedron(e, needle_start, needle_end);
				// Debug.Log("haha");
			}
		}
		//	Debug.Log(CollideTets.Count);
		if (CollideTets.Count > 0)
		{
			return true;            
		}
		return false;
		// Debug.Log(CollideTets.Count);
	}
	
	public bool LineIntersectTetrahedron(element e, Vector3 needle_start, Vector3 needle_end)
	{
		Vector3 CollidePoint; //Debug.Log(e.p[0].pos);
		Vector3[] f1 = { e.p[0].pos, e.p[1].pos, e.p[2].pos };
		Vector3[] f2 = { e.p[0].pos, e.p[1].pos, e.p[3].pos };
		Vector3[] f3 = { e.p[0].pos, e.p[2].pos, e.p[3].pos };
		Vector3[] f4 = { e.p[1].pos, e.p[2].pos, e.p[3].pos };
		
		if (Math3d.intersect3D_RayTriangle(out CollidePoint, needle_start, needle_end, f1[0], f1[1], f1[2]))
		{
			//GameObject.Find("V1").transform.position = CollidePoint;
		}
		
		else if ( Math3d.intersect3D_RayTriangle(out CollidePoint, needle_start, needle_end, f2[0], f2[1], f2[2]))
		{
			//GameObject.Find("V1").transform.position = CollidePoint;
		}
		
		else if ( Math3d.intersect3D_RayTriangle(out CollidePoint, needle_start, needle_end, f3[0], f3[1], f3[2]))
		{
			//GameObject.Find("V1").transform.position = CollidePoint; 
		} 
		
		else if (  Math3d.intersect3D_RayTriangle(out CollidePoint, needle_start, needle_end, f4[0], f4[1], f4[2]))
		{
			//GameObject.Find("V1").transform.position = CollidePoint; 
		} 
		
		
		
		if (Math3d.intersect3D_RayTriangle(out CollidePoint, needle_start, needle_end, f1[0], f1[1], f1[2]) ||
		    Math3d.intersect3D_RayTriangle(out CollidePoint, needle_start, needle_end, f2[0], f2[1], f2[2]) ||
		    Math3d.intersect3D_RayTriangle(out CollidePoint, needle_start, needle_end, f3[0], f3[1], f3[2]) ||
		    Math3d.intersect3D_RayTriangle(out CollidePoint, needle_start, needle_end, f4[0], f4[1], f4[2])
		    )
		{
			// Debug.Log("in");
			CollideTets.Enqueue(e); 
			return true;
		}
		else
		{
			return false;
		}
		
	}
	
	// check whether neigbor collides with needle
	public void CheckNeiborTet(Vector3 needle_start, Vector3 needle_end)
	{
		print ("CheckNeiborTet");
		// element e
		Vector3 centroid,ProjectToNeedle;
		Queue<element> temp_collide = new Queue<element>();
		List<element> Visited = new List<element>(); 
		NeigborAroundCollision = new Queue<element> ();
		while (CollideTets.Count>0)
		{
			element e = CollideTets.Dequeue();
			CollideTets_ForTest.Add(e);
			centroid = (e.p[0].pos + e.p[1].pos + e.p[2].pos + e.p[3].pos) / 4;
			ProjectToNeedle = Math3d.ProjectPointOnLineSegment(needle_start, needle_end, centroid);
			if (Vector3.Distance(centroid, ProjectToNeedle) <= DistanceLimit)
			{
				temp_collide.Enqueue(e);
				NeigborAroundCollision.Enqueue(e);
				foreach (element neighbor in e.elem)
				{
					if (!neighbor.visit)
					{
						
						Visited.Add(neighbor);
						neighbor.visit = true;
						if (LineIntersectTetrahedron(neighbor, needle_start, needle_end))
						{
							CollideTets.Enqueue(neighbor);
							temp_collide.Enqueue(neighbor);
						}
					}
				}
			}            
		}
		CollideTets_ForTest.Clear ();
		CollideTets = new Queue<element>(temp_collide);
		foreach (element e in Visited)
			e.visit = false;
		
		
	}
	
	public Queue<element> ReturnNeigborAroundCollision()
	{
		return NeigborAroundCollision;
	}
}