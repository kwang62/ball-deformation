The purpose of this project is to do unit test in unity and debug my research project.  
CreateMesh.cs is the file that reads a tetrahedral file and draw the surface out of it. HapticInjection.cs is the file that takes the robot (GeomagicTouch)
position and maps to the needle position in the screen. fea_second.cs is the file that calculates the deformation of the ball. The SampleTests.cs
file is the file that do the testing. The source files contained are k.txt, k1.txt, k2.txt, k3.txt, k4.txt, and k5.txt. These are the files I got from this
text book "Matlab guide to finite element". There are also sphere_elements.txt, which is the element label of tetrahedrons of the sphere, sphere_nodes.txt
which contains the 3D coordinates of the sphere, sphere_surface_faces.txt contains the surface triangle of the sphere that needs to be drawn during 
simulation. UnityTest.PNG is a picture that shows how the test GUI looks like.

## Build
This project is built in Unity 4.6.1 and Windows 7. You can download Unity from https://unity3d.com/get-unity/download
The packages that are required for this project is : 
1. Geomagic touch driver : https://www.assetstore.unity3d.com/en/#!/content/34393
2. Unity unit test tool : https://bitbucket.org/Unity-Technologies/unitytesttools/downloads/

## Hardware required
Geomagic touch : https://www.3dsystems.com/haptics-devices/touch

## Create Unity scene
1. Open Unity, Go to : GameObject->Create Empty
2. Attach CreateMesh.cs to the game object
3. Import Geomagic touch package by : Assets->Import Package->Custom Package->Geomagic touch
4. Import unit test package by : Assets->Import Package->Custom Package->Unity Test Tools

## Test project
1. Go to : Unit Test Tools->Unit Tests Runner
2. Click run, after it finishes, you will see all the test with a color on it left. If it passes, it will show as green, otherwise, it is red