using System;
using System.Collections.Generic;
using System.Threading;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;
using System.Linq;
using NUnit.Framework;
using UnityEngine;
using System.IO;
using System.Collections;

namespace UnityTest
{
    [TestFixture]
    [Category("Sample Tests")]
    internal class SampleTests
    {
		// the first element should have a label of -1
		[Test]
		public void TestFirstElementLabel()
		{
            fea_second fea = new fea_second(true, "resources/sphere_nodes.txt");
			Assert.AreEqual(fea_second.Elements [0].label,-1);
		}

		// the first point should have a label of -1
		[Test]
		public void TestFirstPointLabel()
		{
            fea_second fea = new fea_second(true, "resources/sphere_nodes.txt");
			Assert.AreEqual(fea_second.Points [0].label,-1);
		}


		// this test is based on an example from "Matlab guide to finite element", there are 5 elements in this test
		// the answer should be within an threshold my my answer 
        [Test]
        public void TestElementStiffness1()
        {
            string AnsFile = "resources/k1.txt";
            List<double[]> p = points();
            Assert.IsTrue(TestTetrahedronElementStiffness(AnsFile, p[0], p[1], p[3], p[5]));
        }

        [Test]
        public void TestElementStiffness2()
        {
            string AnsFile = "resources/k2.txt";
            List<double[]> p = points();
            Assert.IsTrue(TestTetrahedronElementStiffness(AnsFile, p[0], p[3], p[2], p[6]));
        }

        [Test]
        public void TestElementStiffness3()
        {
            string AnsFile = "resources/k3.txt";
            List<double[]> p = points();
            Assert.IsTrue(TestTetrahedronElementStiffness(AnsFile, p[5], p[4], p[6], p[0]));
        }

        [Test]
        public void TestElementStiffness4()
        {
            string AnsFile = "resources/k4.txt";
            List<double[]> p = points();
            Assert.IsTrue(TestTetrahedronElementStiffness(AnsFile, p[5], p[6], p[7], p[3]));
        }

        [Test]
        public void TestElementStiffness5()
        {
            string AnsFile = "resources/k5.txt";
            List<double[]> p = points();
            Assert.IsTrue(TestTetrahedronElementStiffness(AnsFile, p[0], p[5], p[3], p[6]));
        }
        // store all the points need to be tested
        private List<double[]> points()
        {
            double[] theFirst = { 0.00000, 0.000000, 0.00000 }, theSecond = { 0.02500, 0.00000, 0.00000 },
            theThird = { 0.00000, 0.50000, 0.00000 }, theFourth = { 0.02500, 0.50000, 0.00000 }, theSixth = { 0.02500, 0.00000, 0.25000 },
            theFifth = { 0.00000, 0.00000, 0.25000 }, theSeventh = { 0.00000, 0.50000, 0.25000 }, theEigth = { 0.02500, 0.50000, 0.25000 };
            List<double[]> p = new List<double[]>();
            p.Add(theFirst);
            p.Add(theSecond);
            p.Add(theThird);
            p.Add(theFourth);
            p.Add(theFifth);
            p.Add(theSixth);
            p.Add(theSeventh);
            p.Add(theEigth);
            return p;
        }

        // calculate tetrahedon stiffness matrix and compare to the correct k
        private bool TestTetrahedronElementStiffness(string ans, double[] theFirst, double[] theSecond, double[] theThird, double[] theFourth)
		{
			FileInfo theSourceFile = null;
			theSourceFile = new FileInfo(ans);
            fea_second stiffness = new fea_second(true, "resources/sphere_nodes.txt");
			double E = 210 * Mathf.Pow (10, 6),NU = 0.3;
			
			Matrix<double> k = stiffness.TetrahedronElementStiffness(E, NU, theFirst[0],theFirst[1],theFirst[2],theSecond[0],theSecond[1],theSecond[2],theThird[0],theThird[1],theThird[2],theFourth[0],theFourth[1],theFourth[2]);
			if (k.ColumnCount != 12 || k.RowCount != 12)
								return false;
			
            Matrix<double> k1 = ReadK(ans, 12);
			return CompareBetweenMatrix (k1, k);
		}

        // test whether the global stiffness matrix is correct
		[Test]
		public void TestTetrahedronAssemble()
		{
            string expected = "resources/K.txt";
            Assert.IsTrue(TestGlobalStiffness("resources/k1.txt", "resources/k2.txt", "resources/k3.txt", "resources/k4.txt", "resources/k5.txt", expected));
		}

        // build global stiffness matrix and compare.
        private bool TestGlobalStiffness(string k1_file, string k2_file, string k3_file, string k4_file, string k5_file, string K_file)
		{
            fea_second stiffness = new fea_second(true, "resources/sphere_nodes.txt");
			int local_k_size = 12, global_k_size=24;
			Matrix<double> k1, k2, k3, k4, k5,K,K_tested;
			k1 = ReadK(k1_file,local_k_size);
			k2 = ReadK(k2_file,local_k_size);
			k3 = ReadK(k3_file,local_k_size);
			k4 = ReadK(k4_file,local_k_size);
			k5 = ReadK(k5_file,local_k_size);
			K = ReadK(K_file,global_k_size);
			K_tested = Matrix<double>.Build.Dense (global_k_size,global_k_size);
			K_tested = stiffness.TetrahedronAssemble (K_tested, k1, 1, 2, 4, 6);
			K_tested = stiffness.TetrahedronAssemble (K_tested, k2, 1, 4, 3, 7);
			K_tested = stiffness.TetrahedronAssemble (K_tested, k3, 6, 5, 7, 1);
			K_tested = stiffness.TetrahedronAssemble (K_tested, k4, 6, 7, 8, 4);
			K_tested = stiffness.TetrahedronAssemble (K_tested, k5, 1, 6, 4, 7);
			return CompareBetweenMatrix (K,K_tested);
		}

		// read K matrix from text files
        private Matrix<double> ReadK(string k_file, int k_size)
		{
			Matrix<double> k= Matrix<double>.Build.Dense (k_size, k_size);
			StreamReader reader = ReturnReader (k_file);
			string text = reader.ReadLine ();		
			int lineNum = 0;
			while (text != null)
			{
				string[] arr;
				arr = text.Split(' ');
				for(int i=0;i<k_size;i++)
				{
					k[lineNum,i] = double.Parse(arr[i]);
				}
				
				text = reader.ReadLine();
				lineNum++;
			}
			reader.Close ();
			return k;
		}

        // return the handle of reader
        private StreamReader ReturnReader(string file)
		{
			FileInfo theSourceFile = null;
			theSourceFile = new FileInfo(file);
			
			StreamReader reader = null;
			string text = " "; 
			reader = theSourceFile.OpenText();
			return reader;
		}

        // compare 2 matrices element by element
        private bool CompareBetweenMatrix(Matrix<double> m1, Matrix<double> m2)
		{
			if (m1.RowCount != m2.RowCount || m1.ColumnCount != m2.ColumnCount)
								return false;

			double threshold = Threshold(m1);
			for(int i=0;i<m1.RowCount;i++)
				for(int j=0;j<m1.ColumnCount;j++)
				{
					if(Math.Abs(m1[i,j]-m2[i,j])>=threshold)
						return false;
				} 
			return true;
		}

        // find the max value of the matrix, return threshold. 
        private double Threshold(Matrix<double> m)
        {
            double max = 0;
            for(int i=0;i<m.RowCount;i++)
				for(int j=0;j<m.ColumnCount;j++)
                {
                    if (m[i, j] > max)
                        max = m[i, j];
                }
            return max;
        }

        // test TetForFEA in the file, which is using BFS to find all tetrahedron around the colliding tetrahedron
		[Test]
		public void CheckBFS()
		{
            fea_second fea = new fea_second(true, "resources/sphere_nodes.txt");
			
			fea_second.element collide_tet = fea_second.Elements [1];
			Vector3 needle_end = (collide_tet.p[0].pos+collide_tet.p[1].pos+collide_tet.p[2].pos+collide_tet.p[3].pos)/4;
			List<fea_second.element> Visited = fea.TetForFEA(collide_tet, needle_end);
			List<int> bruteForce = new List<int> ();
			List<int> BFS; 

			/// should be moved out
			BFS = expectedTetforBFS (needle_end,bruteForce,Visited);
			
			Assert.IsTrue (BFS.Count == bruteForce.Count);

			BFS.Sort ();
			bruteForce.Sort ();
			Assert.IsTrue (BFS.SequenceEqual(bruteForce));
		}

		// Calculate the expected value of BFS using brute force
		private List<int> expectedTetforBFS(Vector3 needle_end,List<int> bruteForce,List<fea_second.element> Visited)
		{
			List<int> BFS = new List<int> ();
			for(int i=1;i<fea_second.Elements.Count;i++)
			{
				fea_second.element current = fea_second.Elements[i];
				Vector3 pos = (current.p[0].pos+current.p[1].pos+current.p[2].pos+current.p[3].pos)/4;
				if(Vector3.Distance(pos,needle_end)<fea_second.DistanceLimit)
				{
					bruteForce.Add(current.label);
				}
			}
			
			for(int i = 0;i<Visited.Count;i++)
			{
				BFS.Add(Visited[i].label);
			}

			return BFS;
		}

		// test whether my data structure of storing the negiborhood of a tetrahedron is correct.
		// tetrahedron contains either 3 or 4 neiborhoods, depend on whether it is on the surface or inside
		[Test]
		public void TestNeighborHood()
		{
            fea_second fea = new fea_second(true, "resources/sphere_nodes.txt");
			Assert.Greater(fea_second.Elements.Count,0);
			Assert.Greater (fea_second.Points.Count, 0);
			for(int i=1;i<fea_second.Elements.Count;i++)
			{
				fea_second.element e = fea_second.Elements[i];
				Assert.That(e.elem.Count==3 || e.elem.Count==4);
				HashSet<int> current_tet_label = new HashSet<int> ();

				for(int k=0;k<4;k++)
				{
					current_tet_label.Add(e.p[k].label);
				}

				for(int j=0;j<e.elem.Count;j++)
				{
					fea_second.element neighbor = e.elem[j];
					HashSet<int> neighbor_label = new HashSet<int> ();

					for(int k=0;k<4;k++)
					{
						neighbor_label.Add(neighbor.p[k].label);
					}
					IEnumerable<int> equals = neighbor_label.Intersect(current_tet_label);
					Assert.That(equals.Count()==3);
				}
			}

		}
		 
	
		// Check Collision detection
		[Test]
		public void TestCollisionDetection()
		{
            fea_second fea = new fea_second(false, "resources/sphere_nodes.txt");
			Assert.AreNotEqual(fea_second.Elements,null);
			for(int i=1;i<fea_second.Elements.Count;i++)
			{
				fea_second.element elem = fea_second.Elements[i];
				Vector3 start,end;
				start = elem.p[1].pos;
				end = (elem.p[0].pos+elem.p[1].pos+elem.p[2].pos+elem.p[3].pos)/4;
				fea.CollisionDetection(start, end);
				Assert.IsTrue(fea_second.CollideTets!=null);
				Assert.IsTrue(fea_second.CollideTets.Count>0);
				Assert.IsTrue(fea_second.CollideTets.Peek().label==i);
			}
		}

		// test when a line is inside tetrahedon, whether it will be detected
		[Test]
		public void TestLineIntersectTetrahedron()
		{
            fea_second fea = new fea_second(true, "resources/sphere_nodes.txt");
			for(int i=1;i<fea_second.Elements.Count;i++)
			{
				Vector3 start,end;
				fea_second.element elem = fea_second.Elements[i];
				end = (elem.p[0].pos+elem.p[1].pos+elem.p[2].pos+elem.p[3].pos)/4;
				start = end-5*Vector3.forward;
				Assert.IsTrue(fea.LineIntersectTetrahedron(elem, start, end));
			}
	
		}

		// This test is testing when a line is not intercepting with a tetrahedron, whether it will detect wrongly.
		[Test]
		public void TestLineNotIntersect()
		{
            fea_second fea = new fea_second(true, "resources/sphere_nodes.txt");
			int actual = MaxInterception(fea);
			Assert.IsTrue(actual<5);
		}

		// The line is using brute force going from one point in i-th tetrahedron to the tetrahedron center. It should be collide with
		// the current tetrahedron and 4 of its neighbors. Other tetrahedrons are not part of collision.
		private int MaxInterception(fea_second fea)
		{
			int max_count=0;
			for(int i=1;i<fea_second.Elements.Count-1;i++)
			{
				fea_second.element elem = fea_second.Elements[i];
				Vector3 start,end;
				start = elem.p[1].pos;
				end = (elem.p[0].pos+elem.p[1].pos+elem.p[2].pos+elem.p[3].pos)/4;
				int count = 0;
				for(int j=i+1;j<fea_second.Elements.Count;j++)
				{
					if(!IsNotNeighbor(fea_second.Elements[i],fea_second.Elements[j]))
					{
						if(fea.LineIntersectTetrahedron(fea_second.Elements[j], start, end))
						{
							count++;
						}
					}
				}
				max_count = Math.Max(max_count,count);
			}
			return max_count;
		}

		// checker whether 2 tetrahedrons are neighbor
		private bool IsNotNeighbor(fea_second.element ele1,fea_second.element ele2)
		{
			HashSet<int> L1 = new HashSet<int> ();
			HashSet<int> L2 = new HashSet<int> ();

			for(int i=0;i<4;i++)
			{
				L1.Add(ele1.p[i].label);
				L2.Add(ele2.p[i].label);
			}

			IEnumerable<int> equals = L1.Intersect(L2);
			return equals.Count () == 3;
		}

		// check the postive definiteness of the stiffness matrix
		[Test]
		public void TestPositiveDefinite()
		{
			fea_second fea = new fea_second ();
			Matrix<double> K = fea.ReturnGlobalK ();
			var evd = K.Evd(MathNet.Numerics.LinearAlgebra.Symmetricity.Symmetric);
			Vector<MathNet.Numerics.Complex> eig = evd.EigenValues;
			for(int i=0;i<eig.Count;i++)
			{
				MathNet.Numerics.Complex value = eig[i];
				Assert.IsTrue(value.Imaginary==0);
				Assert.IsTrue(value.Real>0);
			}
		}  

		// test the inverse from Math.Numerics
		[Test]
		public void TestMatrixInverse()
		{
			Matrix<float> input_numerics = Matrix<float>.Build.Dense (4, 4);
			Matrix4x4 input_unity = new Matrix4x4 ();
			float[,] matrix= new float[,] {{-1.2f, -2f, -3f, -4f},{5f, 6f, 7f, 8f},{9f, 10f, 11f, 12f},{13f, 14f, 15f, 160f}}; 
			
			for(int i=0;i<4;i++)
			{
				for(int j=0;j<4;j++)
				{
					input_numerics[i,j] = matrix[i,j];
					input_unity[i,j] = matrix[i,j];
				}
			}
			
			Matrix4x4 expected = input_unity.inverse;
			Matrix<float> actual = input_numerics.Inverse ();
			Assert.AreEqual(expected, actual);
		}


		// test whether the neighborhood of a tetrahedron is being collided
		[Test]
		public void TestCheckNeighborSingleTet()
		{
            fea_second fea = new fea_second(true, "resources/sphere_nodes.txt");
			for(int i=1;i<fea_second.Elements.Count;i++)
			{
				fea_second.element elem = fea_second.Elements[i];
				List<fea_second.element> neighbor = elem.elem;
				Vector3 start = elem.p[1].pos;
				for(int j=0;j<neighbor.Count;j++)
				{
					Vector3 end = (neighbor[j].p[0].pos+neighbor[j].p[1].pos+neighbor[j].p[2].pos+neighbor[j].p[3].pos)/4;
                    Assert.IsTrue(fea.ReturnCheckNeiborForSingleTet(start, end, neighbor[j]));
				}
			}
		}

        // Change the state of an object in the game scene
        [Test]
        public void ChangeGameState()
        {
            GameObject obj = GameObject.Find("AB_neighbor3");
            obj.transform.position = new Vector3(10, 0, 2);
            Assert.AreEqual(obj.transform.position, new Vector3(10, 0, 2));
        }

        // detect whether the change of state will be detected 
        [Test]
        public void DetectGameState()
        {
            GameObject obj = GameObject.Find("AB_neighbor3");
            Assert.AreEqual(obj.transform.position, new Vector3(10, 0, 2));
        }

        // Test the function PickMovingVertex 
        [Test]
        public void TestPickMovingVertex()
        {
            fea_second fea = new fea_second(false, "resources/sphere_nodes.txt");
            List<int> freePointLabel = new List<int>();

            Matrix<double> FreeMatrix, K, PickMatrix;
            int global_k_size = 24, pick_k_size = 12;
            K = ReadK("resources/K.txt", global_k_size);

            freePointLabel.Add(3);
            freePointLabel.Add(4);
            freePointLabel.Add(7);
            freePointLabel.Add(8);

            FreeMatrix = fea.PickMovingVertex(freePointLabel, K);
            PickMatrix = ReadK("resources/K_pick.txt", pick_k_size);

            Assert.IsTrue(CompareBetweenMatrix(FreeMatrix, PickMatrix));
        }


        // Test SetPointsAfterFEA 
        [Test] 
        public void TestSetPointsAfterFEA()
        {
            fea_second fea = new fea_second(false, "resources/sphere_nodes.txt");
            
            List<int> freeTet = new List<int>();

            freeTet.Add(10);
            freeTet.Add(100);

            Vector<float> position_expected = ReadPosFromPoints(fea_second.Points);

            Vector<double> displacement = Vector<double>.Build.Dense(3 * freeTet.Count);           
            for (int i = 0; i < displacement.Count(); i++)
                displacement.Add(i);
            fea.ReturnSetPointsAfterFEA(displacement, freeTet);

            Vector<float> position_actual = ReadPosFromPoints(fea_second.Points);
            Assert.AreEqual(position_expected, position_actual); 
        }

        private Vector<float> ReadPosFromPoints(List<fea_second.point> points)
        {
            Vector<float> pos = Vector<float>.Build.Dense(3 * points.Count);
            for (int i = 0; i < points.Count(); i++)
            {
                pos.Add(points[i].pos[0]);
                pos.Add(points[i].pos[1]);
                pos.Add(points[i].pos[2]);
            }
            return pos;
        }

        // Test NumOfUniquePoints
        [Test]
        public void TestNumOfUniquePoints()
        {
            fea_second fea = new fea_second(false, "resources/sphere_nodes.txt");
            List<fea_second.element> freePointLabel = ConstructElement();
            fea_second.element e;
            List<int> freeTet = new List<int>();
            int actual =  fea.ReturnNumOfUniquePoints(freePointLabel,out freeTet);
            int expected = 3;
            Assert.AreEqual(actual, expected);
        }

        // Construct some elements for test
        private List<fea_second.element> ConstructElement()
        {
            List<fea_second.element> freePointLabel = new List<fea_second.element>();
            fea_second.element e = new fea_second.element();
            
            e.p[0] = new fea_second.point();
            e.p[0].label = 1;
            freePointLabel.Add(e);


            e.p[1] = new fea_second.point();
            e.p[1].label = 2;
            freePointLabel.Add(e);


            e.p[2] = new fea_second.point();
            e.p[2].label = 2;
            freePointLabel.Add(e);


            e.p[3] = new fea_second.point();
            e.p[3].label = 25;
            freePointLabel.Add(e);
            return freePointLabel;
        }

        // test the calculation of the volume of a tetrahedron
        [Test]
        public void TestTetrahedronElementVolume()
        {
            double length,expected_volum,actual_volum;
            double h = 1,area=2*1.0/2;
            expected_volum = 1/3.0*area*h;
            fea_second fea = new fea_second(true, "resources/sphere_nodes.txt");
            actual_volum = fea.ReturnTetrahedronElementVolume(TetPoints());
            Assert.AreEqual(actual_volum, expected_volum);
        }

        private List<double[]> TetPoints()
        {
            double[] theFirst = { 1.0, 0, 0 }, theSecond = { 0, 1, 0 },
            theThird = { 0, -1, 0 }, theFourth = { 0, 0, 1 };
            List<double[]> p = new List<double[]>();
            p.Add(theFirst);
            p.Add(theSecond);
            p.Add(theThird);
            p.Add(theFourth);
            return p;
        } 

        [Test]
        public void TestConvertIndexFromMatlab()
        {
            Matrix<double> m = DenseMatrix.OfArray(new double[,] {
			{1.0, 2, 3, 4},
			{5, 6, 7,8},
			{9, 10, 11, 12},
			{13, 14, 15, 16}});

            double actual = fea_second.ConvertIndexFromMatlab(m, 2, 3);
            double expected = 7;
            Assert.AreEqual(actual, expected);
        }

        [Test]
        public void TestEnforceBoundary()
        {
            List<int> freeTet = new List<int>() {1,3,10,5,100,20,7};
            fea_second fea = new fea_second(true, "resources/sphere_nodes.txt");
            fea_second.element tet = TetForForce();
            Vector<double> ActualBoundaryIndex = fea.ReturnEnforceBoundary(freeTet, tet);
            Vector<double> ExpectedBoundaryIndex = Vector<double>.Build.Dense(4);
            ExpectedBoundaryIndex.Add(1);
            ExpectedBoundaryIndex.Add(3);
            ExpectedBoundaryIndex.Add(5);
            ExpectedBoundaryIndex.Add(6);
            Assert.AreEqual(ActualBoundaryIndex, ActualBoundaryIndex);
        }

        // build an element list with element index of {3,5,20,7}
        fea_second.element TetForForce()
        {
           
            fea_second.element e = new fea_second.element();

            for (int i = 0; i < 4;i++)
            {
                e.p[i] = new fea_second.point();
                if(i==0)
                {
                    e.p[i].label = 3;
                }
                if(i==1)
                    e.p[i].label = 5;
                if(i==2)
                    e.p[i].label = 20;
                if(i==3)
                    e.p[i].label = 7;
                
            }
            return e;
        }

    }
}
